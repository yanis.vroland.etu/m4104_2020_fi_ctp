package fr.ulille.iutinfo.banque;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public  static final String SOLDES_KEY = "Soldes";

    Spinner donneur;
    Spinner receveur;
    EditText montant;
    EditText operation;

    TextView j1Solde;
    TextView j2Solde;
    TextView j3Solde;
    TextView j4Solde;

    private String banque;
    private String joueurs[];
    private int soldeInitial;

    private int[] soldes;

    private Context context = this;

    private class FlingDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if ((Math.abs(velocityX) > Math.abs(velocityY)) && (velocityX < 0)) {

                // TODO Question 2.6 Lancement de la seconde activité
                Serializer.serialize("solde.ser",this,context);
                Intent intent = new Intent(context,VueListe.class);
                intent.putExtra("soldes",soldes);
                intent.putExtra("joueurs",joueurs);
                startActivity(intent);

                // TODO Question 2.7 Communication entre activités



                return true;
            }
            return false;
        }
    }


    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO Question 2.1 Chargement des ressources

        banque = getResources().getString(R.string.nom_banquier);

        MainActivity m = (MainActivity) Serializer.deSerialize("solde.ser",this);
        soldeInitial = getResources().getInteger(R.integer.solde_initial);

        if(m != null){
            soldes = m.getSoldes();
        }else {
            soldes = new int[]{soldeInitial,soldeInitial,soldeInitial,soldeInitial};
        }

        ArrayAdapter adEtudiant = ArrayAdapter.createFromResource(this, R.array.nom_joueurs,R.layout.activity_main);

        joueurs = new String[5];
        joueurs[0] = adEtudiant.getItem(0)+"";
        joueurs[1] = adEtudiant.getItem(1)+"";
        joueurs[2] = adEtudiant.getItem(2)+"";
        joueurs[3] = adEtudiant.getItem(3)+"";
        joueurs[4] = banque+"";

        j1Solde = findViewById(R.id.solde1);
        j2Solde = findViewById(R.id.solde2);
        j3Solde = findViewById(R.id.solde3);
        j4Solde = findViewById(R.id.solde4);



        // TODO Question 2.2 Affichage des noms des joueurs
        TextView j1 = findViewById(R.id.joueur1);
        TextView j2 = findViewById(R.id.joueur2);
        TextView j3 = findViewById(R.id.joueur3);
        TextView j4 = findViewById(R.id.joueur4);

        j1.setText(adEtudiant.getItem(0)+"");
        j2.setText(adEtudiant.getItem(1)+"");
        j3.setText(adEtudiant.getItem(2)+"");
        j4.setText(adEtudiant.getItem(3)+"");


        // TODO Question 2.5 Changement de configuration
        operation = findViewById(R.id.operation);


        // TODO Question 2.3 Initialisation des spinners

        donneur = findViewById(R.id.donneur);
        receveur = findViewById(R.id.receveur);

        ArrayAdapter<CharSequence> addonneur = new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item, joueurs);
        addonneur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        donneur.setAdapter(addonneur);

        ArrayAdapter<CharSequence> adreceveur = new ArrayAdapter<CharSequence>(this,android.R.layout.simple_spinner_item, joueurs);
        adreceveur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        receveur.setAdapter(adreceveur);


        mDetector = new GestureDetectorCompat(this, new FlingDetector());
        update();
    }

    private void update() {
        // TODO Question 2.2 Affichage des soldes des joueurs
        j1Solde.setText(soldes[0]+"");
        j2Solde.setText(soldes[1]+"");
        j3Solde.setText(soldes[2]+"");
        j4Solde.setText(soldes[3]+"");
    }

    public void ajoute_operation(View view) {
        // TODO Question 2.4 ajout d'une transaction
        montant = findViewById(R.id.montant);
        if(!donneur.getSelectedItem().equals(receveur.getSelectedItem()) && montant != null && !montant.getText().equals("0")) {
            if (!donneur.getSelectedItem().equals(banque)) {
                int tmp =  soldes[donneur.getSelectedItemPosition()] - Integer.parseInt(montant.getText()+"");
                if(tmp >=0) {
                    soldes[donneur.getSelectedItemPosition()] = tmp;
                    if (!receveur.getSelectedItem().equals(banque))
                        soldes[receveur.getSelectedItemPosition()] += Integer.parseInt(montant.getText() + "");
                }else{
                    Toast.makeText(this,getResources().getString(R.string.ajout_operation_ko_solde),Toast.LENGTH_SHORT).show();

                }
            }else{
                soldes[receveur.getSelectedItemPosition()] += Integer.parseInt(montant.getText()+"");
            }
            Toast.makeText(this,getResources().getString(R.string.ajout_operation_ok),Toast.LENGTH_SHORT).show();
        }else if(donneur.getSelectedItem().equals(receveur.getSelectedItem())) {
            Toast.makeText(this, getResources().getString(R.string.ajout_operation_ko_cr_db), Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, getResources().getString(R.string.ajout_operation_ko_montant_non_positif), Toast.LENGTH_SHORT).show();
        }
        montant.setText("0");
        update();
    }

    // TODO Question 2.5 Changement de configuration

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    public int[] getSoldes() {
        return soldes;
    }
}
