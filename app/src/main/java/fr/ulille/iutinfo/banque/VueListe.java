package fr.ulille.iutinfo.banque;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class VueListe extends AppCompatActivity {

    int[] soldes;
    String[] joueurs;
    private TextView labelHistorique;
    ArrayAdapter<String> aa;

    class FlingDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if ((velocityX > velocityY) && (velocityX > 0)) {
                finish();
                return true;
            }
            return false;
        }
    }

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vue_liste);

        joueurs = getResources().getStringArray(R.array.nom_joueurs);

        // TODO Querestion 2.7 cimmunications entre activités
        Bundle bundle = getIntent().getExtras();
        soldes = (int[]) bundle.get("soldes");
        joueurs = (String[]) bundle.get("joueurs");


        // TODO Questino 2.8 Construction de la listView

        aa = new MonTacheAdapter();
    }

    class MonTacheAdapter extends ArrayAdapter {
        MonTacheAdapter() {
            super(VueListe.this, R.layout.item_liste_joueurs, joueurs);

        }
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=getLayoutInflater();
            View row=inflater.inflate(R.layout.item_liste_joueurs, parent, false);
            TextView name=(TextView)row.findViewById(R.id.nom);
            name.setText(joueurs[position]);
            TextView sol=(TextView)row.findViewById(R.id.solde);
            sol.setText(soldes[position]);
            return  row;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }
}
